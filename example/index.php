<?php
require __DIR__ . '/../vendor/autoload.php';
use P3dr0\Fpdf\Pdf;
$pdf=new Pdf();
$pdf->addPage();
$pdf->SetWidths([30,80]);
$pdf->SetFont('Arial','',9);
$pdf->Cell(200,5,'METODO ROW',0,0,'L',false);
$pdf->Ln(5);
$pdf->Row([
    'ROW 30 WIDTH',
    'ROW 80 WIDTH'
]);
$pdf->Ln(5);
$pdf->Cell(200,5,'WriteHTML',0,0,'L',false);
$pdf->Ln(5);
$pdf->WriteHTML('ESTO ES UN PARRAFO CON <b>NEGRITA</b> <u>CURSIVA</u>');
$pdf->Ln(8);
$pdf->Cell(200,5,'WriteTable',0,0,'L',false);
$pdf->Ln(5);
$columns=[];
$col =[];
$col[] = array( 'text' => 'WRITE TABLE 30 WIDTH', 'width' => '30', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'LBTR');
$col[] = array( 'text' => 'WRITE TABLE 80 WIDTH', 'width' => '80', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.2', 'linearea' => 'RBT');
$columns[] = $col;
$pdf->WriteTable($columns);
$pdf->Ln(8);
$pdf->Cell(200,5,'CODE128',0,0,'L',false);
$pdf->Ln(5);
$pdf->SetFillColor(0, 0, 0);
$pdf->Code128(30,60,'123456789',35,10);
$pdf->Output();